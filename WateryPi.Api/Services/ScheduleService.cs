﻿using WateryPi.Api.Models;

namespace WateryPi.Api.Services
{
    public class ScheduleService : IScheduleService
    {
        public async Task<IList<Schedule>> GetSchedulesAsync()
        {
            return new List<Schedule>
            {
                new Schedule
                {
                    Id = Guid.NewGuid(),
                    DurationSeconds = 23,
                    CronExpression = "Every Monday"
                },
                new Schedule
                {
                    Id = Guid.NewGuid(),
                    DurationSeconds = 53,
                    CronExpression = "Midnight every Saturday"
                }
            };
        }        
    }
}
