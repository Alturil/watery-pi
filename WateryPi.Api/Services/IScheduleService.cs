﻿using WateryPi.Api.Models;

namespace WateryPi.Api.Services
{
    public interface IScheduleService
    {
        public Task<IList<Schedule>> GetSchedulesAsync();
    }
}
