﻿using System.Runtime.Serialization;

namespace WateryPi.Api.Models
{
    [DataContract]
    public class Schedule
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public int DurationSeconds { get; set; }

        [DataMember]
        public string CronExpression { get; set; }
    }
}
