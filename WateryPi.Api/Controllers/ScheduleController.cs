﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WateryPi.Api.Models;
using WateryPi.Api.Services;

namespace WateryPi.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduleController : ControllerBase
    {
        private readonly IScheduleService _scheduleService;

        public ScheduleController(IScheduleService scheduleService)
        {
            _scheduleService = scheduleService;
        }


        [HttpGet(Name = "GetSchedules")]
        public async Task<IList<Schedule>> Get()
        {
            return await _scheduleService.GetSchedulesAsync();
        }
    }
}
